/**
 * Payment.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Class used to handle 'payments' table records.
 * 
 * @author felipedabrantes
 *
 */
public class Payment implements Comparable<Payment> {
	
	/** The customer associated with the payment. */
	private int customerNumber = 0;
	/** The check number uniquely identifies the payment. */
	private String checkNumber = null;
	/** The date the payment was processed. */
	private Date paymentDate = null;
	/** Amount paid. */
	private double amount = 0.0;
	
	/** Used when displaying prices. Formats doubles to 2 decimal places. */
	public static final DecimalFormat PRICE_FORMAT = new DecimalFormat("0.00");
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param customerNumber
	 * @param checkNumber
	 * @param paymentDate
	 * @param amount
	 */
	public Payment(int customerNumber, String checkNumber, Date paymentDate, double amount) {
		this.customerNumber = customerNumber;
		this.checkNumber = checkNumber;
		this.paymentDate = paymentDate;
		this.amount = amount;
	}
	
	
	/**
	 * 
	 * Constructor. Initialises fields with a given result set from a table.
	 *
	 * @param tableRecord result set. Constructor deals with reading record.
	 * 
	 * @throws SQLException if column names do not exist in table.
	 */
	public Payment(ResultSet tableRecord) throws SQLException {
		this.customerNumber = tableRecord.getInt("customerNumber");
		this.checkNumber = tableRecord.getString("checkNumber");
		this.paymentDate = tableRecord.getDate("paymentDate");
		this.amount = tableRecord.getDouble("amount");
	}
	
	
	/**
	 * @return the customer
	 */
	public int getCustomerNumber() {
		return this.customerNumber;
	}
	
	/**
	 * @return the checkNumber
	 */
	public String getCheckNumber() {
		return this.checkNumber;
	}
	
	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return this.paymentDate;
	}
	
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return this.amount;
	}


	/**
	 * A class function used to find the average amount of a given list of payments.
	 *  
	 * @param list of payments to find average of.
	 * 
	 * @return average amount.
	 */
	static public double findAverageAmount(List<Payment> payments) {
		double totalAmount = 0.0;
		int paymentsNum = payments.size();
		
		for(Payment payment: payments) {
			totalAmount += payment.getAmount();
		}
		
		return(totalAmount/paymentsNum);
	}
	
	
	/**
	 * A class function used to find the payments with amounts greater than a multiple of the average.
	 * For example, if the multiple parameter is 2.0, the function returns payments with amounts of an average greater than twice the average.
	 * 
	 * @param list of payments to analyse.
	 * @param multiple of the average the amount must be greater than.
	 * 
	 * @return a list of payments that are above the multiple of the average.
	 */
	static public List<Payment> findGreaterThanAverage(List<Payment> payments, double multiple) {
		//Finds average of payments given.
		double averageAmount = Payment.findAverageAmount(payments);
		
		//Check if payments are greater than this value:
		double threshold = averageAmount * multiple;
		
		List<Payment> paymentsGreaterThanAverage = new ArrayList<Payment> ();
		
		for(Payment payment: payments) {
			if(payment.getAmount() > threshold) {
				paymentsGreaterThanAverage.add(payment);
			}
		}
		
		//Sorts payments list by the amount attribute of payments.
		Collections.sort(paymentsGreaterThanAverage);
		
		return paymentsGreaterThanAverage;
	}
	
	
	/**
	 * Returns the payment check number, amount and customer number when called or printed in the console.
	 */
	@Override
	public String toString() {
		return (this.getCheckNumber() + " £" + Payment.PRICE_FORMAT.format(this.getAmount()));
	}
	
	
	/**
	 * Objects of the Payment class are now sorted by their amount, from low to high.
	 */
	@Override
	public int compareTo(Payment otherPayment) {
		return (Double.valueOf(this.getAmount()).compareTo(Double.valueOf(otherPayment.getAmount())));
	}
}
