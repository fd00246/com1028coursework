/**
 * OrderDetails.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class used to handle 'orderdetails' table records.
 * 
 * @author felipedabrantes
 *
 */
public class OrderDetails {
	
	/** Unique order number linked to order details. */
	private int orderNumber = 0;
	/** Unique product code linked to order. */
	private String productCode = null;
	/** Quantity ordered of product. */
	private int quantityOrdered = 0;
	/** Price of each product. */
	private double priceEach = 0.0;
	/** Line number of order. */
	private int orderLineNumber = 0;
	
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param orderNumber
	 * @param productCode
	 * @param quantityOrdered
	 * @param priceEach
	 * @param orderLineNumber
	 */
	public OrderDetails(int orderNumber, String productCode, int quantityOrdered, double priceEach,
			int orderLineNumber) {
		
		this.orderNumber = orderNumber;
		this.productCode = productCode;
		this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
		this.orderLineNumber = orderLineNumber;
	}
	
	
	/**
	 * 
	 * Constructor. Initialises fields with a given result set from a table.
	 *
	 * @param tableRecord result set. Constructor deals with reading record.
	 * 
	 * @throws SQLException if column names do not exist in table.
	 */
	public OrderDetails(ResultSet tableRecord) throws SQLException {
		this.orderNumber = tableRecord.getInt("orderNumber");
		this.productCode = tableRecord.getString("productCode");
		this.quantityOrdered = tableRecord.getInt("quantityOrdered");
		this.priceEach = tableRecord.getDouble("priceEach");
		this.orderLineNumber = tableRecord.getInt("orderLineNumber");
	}


	/**
	 * @return the orderNumber
	 */
	public int getOrderNumber() {
		return this.orderNumber;
	}


	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return this.productCode;
	}


	/**
	 * @return the quantityOrdered
	 */
	public int getQuantityOrdered() {
		return this.quantityOrdered;
	}


	/**
	 * @return the priceEach
	 */
	public double getPriceEach() {
		return this.priceEach;
	}


	/**
	 * @return the orderLineNumber
	 */
	public int getOrderLineNumber() {
		return this.orderLineNumber;
	}
	
	
	/**
	 * Returns the number order and the corresponding product code when printed on the console.
	 */
	@Override
	public String toString() {
		return ("#" + this.getOrderNumber() +  " -> Product: #" + this.getProductCode());
	}
}
