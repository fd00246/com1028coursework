/**
 * Order.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class used to handle 'orders' table records.
 * 
 * @author felipedabrantes
 *
 */
public class Order {
	
	/** Order number to uniquely identify order. */
	private int orderNumber = 0;
	/** Date the order was processed. */
	private Date orderDate = null;
	/** Date the order should arrive. */
	private Date requiredDate = null;
	/** Date the order is shipped. */
	private Date shippedDate = null;
	/** Status of order. */
	private String status = null;
	/** Comments about the order. */
	private String comments = null;
	/** Customer number associated with order. */
	private int customerNumber = 0;
	
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param orderNumber
	 * @param orderDate
	 * @param requiredDate
	 * @param shippedDate
	 * @param status
	 * @param comments
	 * @param customerNumber
	 */
	public Order(int orderNumber, Date orderDate, Date requiredDate, Date shippedDate, String status, 
			String comments, int customerNumber) {
		
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.comments = comments;
		this.customerNumber = customerNumber;
	}
	
	
	/**
	 * 
	 * Constructor. Initialises fields with a given result set from a table.
	 *
	 * @param tableRecord result set. Constructor deals with reading record.
	 * 
	 * @throws SQLException 
	 */
	public Order(ResultSet tableRecord) throws SQLException {
		this.orderNumber = tableRecord.getInt("orderNumber");
		this.orderDate = tableRecord.getDate("orderDate");
		this.requiredDate = tableRecord.getDate("requiredDate");
		this.shippedDate = tableRecord.getDate("shippedDate");
		this.status = tableRecord.getString("status");
		this.comments = tableRecord.getString("comments");
		this.customerNumber = tableRecord.getInt("customerNumber");
	}
	

	/**
	 * @return the orderNumber
	 */
	public int getOrderNumber() {
		return this.orderNumber;
	}


	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return this.orderDate;
	}


	/**
	 * @return the requiredDate
	 */
	public Date getRequiredDate() {
		return this.requiredDate;
	}


	/**
	 * @return the shippedDate
	 */
	public Date getShippedDate() {
		return this.shippedDate;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return this.comments;
	}


	/**
	 * @return the customer
	 */
	public int getCustomerNumber() {
		return this.customerNumber;
	}


	/**
	 * Finds orders with given status.
	 * 
	 * @param orders list to analyse.
	 * @param status of orders wanted.
	 * 
	 * @return list of orders with status given.
	 */
	static public List<Order> findOrdersOnStatus(List<Order> orders, String status) {
		List<Order> ordersOnHold = new ArrayList<Order> ();
		for(Order order: orders) {
			if(order.getStatus().equals(status)) {
				ordersOnHold.add(order);
			}
		}
		
		return ordersOnHold;
	}
	
	
	/**
	 * Returns the number order with a # in front when printed in console.
	 */
	@Override
	public String toString() {
		return("#" + String.valueOf(this.getOrderNumber()));
	}
}
