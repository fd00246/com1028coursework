/**
 * Customer.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Class used to handle 'customers' table records.
 * 
 * @author felipedabrantes
 *
 */
public class Customer implements Comparable<Customer>{
	
	/** Customer's number to uniquely identify customer. */
	private int customerNumber = 0;
	/** Customer's full name. */
	private String customerName = null;
	/** Customer's contact's last name. */
	private String contactLastName = null;
	/** Customer's contact's first name. */
	private String contactFirstName = null;
	/** Customer's phone number. Includes symbols. */
	private String phone = null;
	/** Customer's main address. */
	private String addressLine1 = null;
	/** Customer's main address extra. */
	private String addressLine2 = null;
	/** Customer's city name. */
	private String city = null;
	/** Customer's state name. */
	private String state = null;
	/** Customer's postal code. */
	private String postalCode = null;
	/** Customer's country name. */
	private String country = null;
	/** Customer's sales representative employee number. */
	private int salesRepEmployeeNumber = 0;
	/** Customer's credit limit. */
	private double creditLimit = 0.0;
	
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param customerNumber
	 * @param customerName
	 * @param contactLastName
	 * @param contactFirstName
	 * @param phone
	 * @param addressLine1
	 * @param addressLine2
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param country
	 * @param salesRepEmployeeNumber
	 * @param creditLimit
	 */
	public Customer(int customerNumber, String customerName, String contactLastName, String contactFirstName,
			String phone, String addressLine1, String addressLine2, String city, String state, String postalCode,
			String country, int salesRepEmployeeNumber, double creditLimit) {
		
		this.customerNumber = customerNumber;
		this.customerName = customerName;
		this.contactLastName = contactLastName;
		this.contactFirstName = contactFirstName;
		this.phone = phone;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.country = country;
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
		this.creditLimit = creditLimit;
	}
	
	
	/**
	 * 
	 * Constructor. Initialises fields with a given result set from a table.
	 *
	 * @param tableRecord result set. Constructor deals with reading record.
	 * 
	 * @throws SQLException 
	 */
	public Customer(ResultSet tableRecord) throws SQLException {
		
		this.customerNumber = tableRecord.getInt("customerNumber");
		this.customerName = tableRecord.getString("customerName");
		this.contactLastName = tableRecord.getString("contactLastName");
		this.contactFirstName = tableRecord.getString("contactFirstName");
		this.phone = tableRecord.getString("phone");
		this.addressLine1 = tableRecord.getString("addressLine1");
		this.addressLine2 = tableRecord.getString("addressLine2");
		this.city = tableRecord.getString("city");
		this.state = tableRecord.getString("state");
		this.postalCode = tableRecord.getString("postalCode");
		this.country = tableRecord.getString("country");
		this.salesRepEmployeeNumber = tableRecord.getInt("salesRepEmployeeNumber");
		this.creditLimit = tableRecord.getDouble("creditLimit");
	}
	
	
	/**
	 * @return the customerNumber
	 */
	public int getCustomerNumber() {
		return this.customerNumber;
	}


	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return this.customerName;
	}


	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return this.contactLastName;
	}


	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName() {
		return this.contactFirstName;
	}


	/**
	 * @return the phone
	 */
	public String getPhone() {
		return this.phone;
	}


	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return this.addressLine1;
	}


	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return this.addressLine2;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return this.city;
	}


	/**
	 * @return the state
	 */
	public String getState() {
		return this.state;
	}


	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return this.postalCode;
	}


	/**
	 * @return the country
	 */
	public String getCountry() {
		return this.country;
	}


	/**
	 * @return the salesRepEmployeeNumber
	 */
	public int getSalesRepEmployeeNumber() {
		return this.salesRepEmployeeNumber;
	}


	/**
	 * @return the creditLimit
	 */
	public double getCreditLimit() {
		return this.creditLimit;
	}


	/**
	 * A function to find the customer object associated with the given customer number.
	 * 
	 * @param customerNumber
	 * @param customers list of the customer objects.
	 * 
	 * @return customer object.
	 * 
	 * @throws IllegalArgumentException if no customer object is found associated with given customer number.
	 */
	public static Customer findCustomer(List<Customer> customers, int customerNumber) throws IllegalArgumentException{
		
		Customer customerFound = null;
		
		for(Customer customer: customers) {
			if(customer.getCustomerNumber() == customerNumber) {
				customerFound = customer;
				break;
			}
		}
		
		if (customerFound == null) {
			throw new IllegalArgumentException("No customer found associated with the given customer number.");
		}
		
		return customerFound;
	}
	
	
	/**
	 * A function to return the full info of a customer.
	 * 
	 * @return full info as a string.
	 */
	public String getFullInfo() {
		StringBuffer fullInfo = new StringBuffer();
		
		fullInfo.append("\n-> Contact:\n  - ");
		fullInfo.append(this.getContactLastName());
		fullInfo.append(", ");
		fullInfo.append(this.getContactFirstName());
		fullInfo.append(" (");
		fullInfo.append(this.getPhone());
		fullInfo.append(")\n\n-> Address:");
		if(this.getAddressLine1() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getAddressLine1());
		}
		if(this.getAddressLine2() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getAddressLine2());
		}
		if(this.getCity() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getCity());
		}
		if(this.getState() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getState());
		}
		if(this.getPostalCode() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getPostalCode());
		}
		if(this.getCountry() != null) {
			fullInfo.append("\n  - ");
			fullInfo.append(this.getCountry());
		}
		fullInfo.append("\n\n-> Info:\n  - Sales Rep Employee Num: #");
		fullInfo.append(this.getSalesRepEmployeeNumber());
		fullInfo.append("\n  - Credit Limit: £");
		fullInfo.append(Payment.PRICE_FORMAT.format(this.getCreditLimit()));
		fullInfo.append("\n");
		
		return fullInfo.toString();
	}
	
	
	/**
	 * Returns customer name and customer number.
	 */
	@Override
	public String toString() {
		return this.getCustomerName() + " (#" + this.getCustomerNumber() + ")";
	}
	
	
	/**
	 * Objects of the Customer class are now sorted by their customer name, alphabetically.
	 */
	@Override
	public int compareTo(Customer otherCustomer) {
		return (this.getCustomerName().compareToIgnoreCase(otherCustomer.getCustomerName()));
	}
}
