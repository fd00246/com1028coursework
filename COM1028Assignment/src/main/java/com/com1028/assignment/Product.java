/**
 * Product.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Class used to handle 'products' table records.
 * 
 * @author felipedabrantes
 *
 */
public class Product implements Comparable<Product>{
	
	/** The product unique code. */
	private String productCode = null;
	/** The product name. */
	private String productName = null;
	/** The product line. */
	private String productLine = null;
	/** The product scale. */
	private String productScale = null;
	/** The product vendor, supplier. */
	private String productVendor = null;
	/** The product description. */
	private String productDescription = null;
	/** Quantity of product in stock. */
	private int quantityInStock = 0;
	/** Buy price of product. */
	private double buyPrice = 0.0;
	/** Manufacturer's suggested retail price. */
	private double MSRP = 0.0;
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param productCode
	 * @param productName
	 * @param productLine
	 * @param productScale
	 * @param productVendor
	 * @param productDescription
	 * @param quantityInStock
	 * @param buyPrice
	 * @param MSRP
	 */
	public Product(String productCode, String productName, String productLine, String productScale,
			String productVendor, String productDescription, int quantityInStock, double buyPrice, double MSRP) {

		this.productCode = productCode;
		this.productName = productName;
		this.productLine = productLine;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.MSRP = MSRP;
	}
	
	
	/**
	 * 
	 * Constructor. Initialises fields with a given result set from a table.
	 *
	 * @param tableRecord result set. Constructor deals with reading record.
	 * 
	 * @throws SQLException if column names do not exist in table.
	 */
	public Product(ResultSet tableRecord) throws SQLException {
		this.productCode = tableRecord.getString("productCode");
		this.productName = tableRecord.getString("productName");
		this.productLine = tableRecord.getString("productLine");
		this.productScale = tableRecord.getString("productScale");
		this.productVendor = tableRecord.getString("productVendor");
		this.productDescription = tableRecord.getString("productDescription");
		this.quantityInStock = tableRecord.getInt("quantityInStock");
		this.buyPrice = tableRecord.getDouble("buyPrice");
		this.MSRP = tableRecord.getDouble("MSRP");
	}


	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return this.productCode;
	}


	/**
	 * @return the productName
	 */
	public String getProductName() {
		return this.productName;
	}


	/**
	 * @return the productLine
	 */
	public String getProductLine() {
		return this.productLine;
	}


	/**
	 * @return the productScale
	 */
	public String getProductScale() {
		return this.productScale;
	}


	/**
	 * @return the productVendor
	 */
	public String getProductVendor() {
		return this.productVendor;
	}


	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return this.productDescription;
	}


	/**
	 * @return the quantityInStock
	 */
	public int getQuantityInStock() {
		return this.quantityInStock;
	}


	/**
	 * @return the buyPrice
	 */
	public double getBuyPrice() {
		return this.buyPrice;
	}


	/**
	 * @return the MSRP
	 */
	public double getMSRP() {
		return this.MSRP;
	}
	
	
	/**
	 * A function to find the product object associated with the given product code.
	 * 
	 * @param productCode
	 * @param products list of the product objects.
	 * 
	 * @return product object.
	 * 
	 * @throws IllegalArgumentException if no product object is found associated with given product code.
	 */
	public static Product findProduct(List<Product> products, String productCode) throws IllegalArgumentException{
		
		Product productFound = null;
		
		for(Product product: products) {
			if(product.getProductCode().equals(productCode)) {
				productFound = product;
				break;
			}
		}
		
		//If no product found, throw IllegalArgumentException.
		if (productFound == null) {
			throw new IllegalArgumentException("No product found associated with the given product code.");
		}
		
		return productFound;
	}
	
	
	/**
	 * Returns the product name and code when called or printed in the console.
	 */
	@Override
	public String toString() {
		return (this.getProductName() + " (#" + this.getProductCode() + ")");
	}
	
	
	/**
	 * Objects of the Product class are now sorted by their product name, alphabetically.
	 */
	@Override
	public int compareTo(Product otherProduct) {
		return (this.getProductName().compareToIgnoreCase(otherProduct.getProductName()));
	}

}
