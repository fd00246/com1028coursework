/**
 * MainSystem.java
 */
package com.com1028.assignment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * A class to use as a control of all the imported records from the database.
 * 
 * @author felipedabrantes
 *
 */
public class MainSystem {
	/** Base query used to access the database. */
	private BaseQuery baseQuery = null;
	
	/** List of customers in the system. */
	private List<Customer> customers = new ArrayList<Customer> ();
	/** List of payments in the system. */
	private List<Payment> payments = new ArrayList<Payment> ();
	/** List of orders in the system. */
	private List<Order> orders = new ArrayList<Order> ();
	/** List of order details in the system. */
	private List<OrderDetails> orderDetails = new ArrayList<OrderDetails> ();
	/** List of products in the system. */
	private List<Product> products = new ArrayList<Product> ();
	
	
	/**
	 * 
	 * Constructor. Connects to the database.
	 *
	 * @param username of the database server.
	 * @param password of the database server.
	 */
	public MainSystem(String username, String password) {
		this.baseQuery = new BaseQuery(username, password);
	}
	
	
	/**
	 * A function to display a report of the payments greater than twice the average.
	 * 
	 * @return report as a string.
	 */
	public String paymentsGreaterThanTwiceTheAverageReport() {
		//Finds list of payments greater than double the average.
		List<Payment> paymentsGreaterDoubleAverage = Payment.findGreaterThanAverage(this.payments, 2);
		
		//Creates report:
		StringBuffer report = new StringBuffer();
		
		report.append("\n---Payments Greater Than Twice The Average---\n");
		for(Payment payment: paymentsGreaterDoubleAverage) {
			report.append("\n");
			report.append(payment);
			report.append(" -> ");
			report.append(Customer.findCustomer(getCustomers(), payment.getCustomerNumber()));
		}

		return report.toString();
	}


	/**
	 * A function to display a report on the orders of the status given for each customer.
	 * 
	 * @param status of the orders desired.
	 * 
	 * @return report as a string.
	 */	
	public String ordersStatusReport(String status) {
		//Finds all orders with the status given.
		List<Order> ordersOnStatus = Order.findOrdersOnStatus(this.orders, status);
		
		//A hash map connecting the customer to their orders with the status given.
		Map<Customer, List<Order>> customersOnStatus = new TreeMap<Customer, List<Order>> ();
		
		Customer customerWithStatus = null;
		
		//Iterates orders and adds info to hash map.
		for(Order order: ordersOnStatus) {
			List<Order> tempOrders = new ArrayList<Order> ();
			customerWithStatus = Customer.findCustomer(this.customers, order.getCustomerNumber());
			
			//If customer is not on map, add it and the order.
			if(customersOnStatus.get(customerWithStatus) == null) {
				tempOrders.add(order);
				customersOnStatus.put(customerWithStatus, tempOrders);
			}
			
			//If customer is on map, add the order to the order list.
			else {
				tempOrders = customersOnStatus.get(customerWithStatus);
				tempOrders.add(order);
				customersOnStatus.put(customerWithStatus, tempOrders);
			}
		}
		
		//Writing report data.
		StringBuffer report = new StringBuffer();
		report.append("\n-*-*-*- Orders ");
		report.append(status);
		report.append(" Report -*-*-*-");
		report.append("\n\n-> Total number of customers with Orders ");
		report.append(status);
		report.append(": ");
		report.append(customersOnStatus.size());
		report.append("\n");
		for(Entry<Customer, List<Order>> entry: customersOnStatus.entrySet()) {
			report.append("\n\n--* ");
			report.append(entry.getKey());
			report.append(" *--\n-> ");
			report.append(entry.getValue().size());
			report.append(" Orders ");
			report.append(status);
			report.append(":");
			
			for(Order order: entry.getValue()) {
				report.append("\n  - ");
				report.append(order);
			}
			report.append("\n");
			report.append(entry.getKey().getFullInfo());
		}
		
		//Returns string buffer as string.
		return(report.toString());
	}
	
	
	/**
	 * A function to find the total revenue of each product in a report.
	 * 
	 * @return a report of the products and revenue made from each.
	 */
	public String revenueOfProductsReport() {
		//Map containing the product and profit made from it.
		//Using a TreeMap sorts keys by product's compareTo function automatically.
		//The product's compareTo function sorts products alphabetically.
		Map<Product, Double> productsRevenue = new TreeMap<Product, Double> ();
		
		for(OrderDetails currentOrderDetails: this.orderDetails) {
			//Find product sold of the corresponding order detail.
			Product productSold = Product.findProduct(this.products, currentOrderDetails.getProductCode());
			
			//Revenue = (price) * (quantity sold)
			Double sellingPriceOfProductSold = currentOrderDetails.getPriceEach();
			int quantityOfProductSold = currentOrderDetails.getQuantityOrdered();
			
			Double revenueOfProductSold = quantityOfProductSold * sellingPriceOfProductSold;
			
			
			//Using a TreeMap does not allow null values to be stored.
			try {
				//If no key is found with the variable in the get function, a NullPointerException is thrown.
				productsRevenue.get(productSold);
				
				//If no NullPointerException is thrown, the product key is already in map.
				//Add the new revenue to the product value in the map.
				Double previousRevenue = productsRevenue.get(productSold);
				productsRevenue.put(productSold, previousRevenue + revenueOfProductSold);
			}
			catch(NullPointerException e) {
				//If NullPointerException is raised, put product as new key in map.
				productsRevenue.put(productSold, revenueOfProductSold);
			}
			
		}
		
		//Writing report data.
		StringBuffer report = new StringBuffer();
		report.append("\n---Product Revenue---\n");
		for(Entry<Product, Double> entry: productsRevenue.entrySet()) {
			report.append("\n");
			report.append(entry.getKey().toString());
			report.append(" -> £");
			report.append(Payment.PRICE_FORMAT.format(entry.getValue()));
		}
		
		return (report.toString());
	}
	
	
	/**
	 * A function to remove the given customer from the system.
	 * 
	 * @param customer to remove.
	 */
	public void removeCustomerFromSystem(Customer customer) {
		this.getCustomers().remove(customer);
		customer = null;
	}
	
	
	/**
	 * @return the customers
	 */
	public List<Customer> getCustomers() {
		return this.customers;
	}


	/**
	 * Adds customers from the given table name.
	 * 
	 * @param tableName to add customers from.
	 * 
	 * @throws SQLException if no table with the name given was found.
	 */
	public void addCustomersFromTable(String tableName) throws SQLException {
		ResultSet tableResults = baseQuery.useTable(tableName);
		
		Customer tempCustomer = null;
		
		while(tableResults.next()) {			
			tempCustomer = new Customer(tableResults);
			this.customers.add(tempCustomer);
		}
	}
	
	
	/**
	 * Adds payments from the given table name.
	 * 
	 * @param tableName to add payments from.
	 * 
	 * @throws SQLException if no table with the name given was found.
	 */
	public void addPaymentsFromTable(String tableName) throws SQLException {
		ResultSet tableResults = baseQuery.useTable(tableName);
		
		Payment tempPayment = null;
		
		while(tableResults.next()) {			
			tempPayment = new Payment(tableResults);
			this.payments.add(tempPayment);
		}
	}
	
	
	/**
	 * Adds orders from the given table name.
	 * 
	 * @param tableName to add orders from.
	 * 
	 * @throws SQLException if no table with the name given was found.
	 */
	public void addOrdersFromTable(String tableName) throws SQLException {
		ResultSet tableResults = baseQuery.useTable(tableName);
		
		Order tempOrder = null;
		
		while(tableResults.next()) {			
			tempOrder = new Order(tableResults);
			this.orders.add(tempOrder);
		}
	}
	
	
	/**
	 * Adds order details from the given table name.
	 * 
	 * @param tableName to add order details from.
	 * 
	 * @throws SQLException if no table with the name given was found.
	 */
	public void addOrderDetailsFromTable(String tableName) throws SQLException {
		ResultSet tableResults = baseQuery.useTable(tableName);
		
		OrderDetails tempOrderDetails = null;
		
		while(tableResults.next()) {			
			tempOrderDetails = new OrderDetails(tableResults);
			this.orderDetails.add(tempOrderDetails);
		}
	}
	
	
	/**
	 * Adds products from the given table name.
	 * 
	 * @param tableName to add products from.
	 * 
	 * @throws SQLException if no table with the name given was found.
	 */
	public void addProductsFromTable(String tableName) throws SQLException {
		ResultSet tableResults = baseQuery.useTable(tableName);
		
		Product tempProduct = null;
		
		while(tableResults.next()) {			
			tempProduct = new Product(tableResults);
			this.products.add(tempProduct);
		}
	}
}
