/**
 * BaseQuery.java
 */
package com.com1028.assignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A class to access the database server.
 *
 */
public class BaseQuery {
	//Establishes connection to database server.
	protected Connection con;
	private final String db = "jdbc:mysql://localhost:3306/classicmodels";

	
	/**
	 * 
	 * Constructor. Accesses database with given username and password.
	 *
	 * @param uname
	 * @param pwd
	 */
	public BaseQuery(String uname, String pwd){
		try {
		    //Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			con = DriverManager.getConnection( db, uname, pwd);
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	
	/**
	 * Access table records of the given table name.
	 * 
	 * @param tableName of the table records wanted.
	 * 
	 * @return the database result set of the given table.
	 * 
	 * @throws SQLException if no table exists with given name.
	 */
	protected ResultSet useTable(String tableName) throws SQLException{
		String query = "select * from " + tableName; 
		Statement s = con.createStatement();
		ResultSet rs = s.executeQuery(query);
		return rs;
	}
	
	
	/**
	 * Runs query given in parameter.
	 * 
	 * @param query to run.
	 * 
	 * @return the database result set.
	 * 
	 * @throws SQLException if SLQ query is invalid.
	 */
	protected ResultSet runQuery(String query) throws SQLException{
		Statement s = con.createStatement();
		ResultSet rs = s.executeQuery(query);
		return rs;
	}
	


}
