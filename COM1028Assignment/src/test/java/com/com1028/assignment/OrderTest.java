/**
 * OrderTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the Order class.
 * 
 * @author felipedabrantes
 *
 */
public class OrderTest {
	// Multiple order objects to test for functions where a list is needed. */
	private Order firstOrder = null;
	private Order secondOrder = null;
	private Order thirdOrder = null;
	
	/* 
	 * Different dates to test different order dates: ordered, required, shipped.
	 * Dates are 1 second apart, starting from 1 second after 1/1/1970.
	 */
	private Date firstDate = new Date(1000);
	private Date secondDate = new Date(2000);
	private Date thirdDate = new Date(3000);
	
	
	/**
	 * Called before every test case to initialise fields and load resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		firstOrder = new Order(1, firstDate, secondDate, thirdDate, "On Hold", null, 0);
		secondOrder = new Order(2, firstDate, secondDate, thirdDate, "On Hold", null, 0);
		thirdOrder = new Order(3, firstDate, secondDate, thirdDate, "Shipped", null, 0);
	}

	/**
	 * Called after the end of each test case to release resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		firstOrder = null;
		secondOrder = null;
		thirdOrder = null;
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Order#Order(int, java.util.Date, java.util.Date, java.util.Date, java.lang.String, java.lang.String, int)}.
	 */
	@Test
	public void testOrderConstructor() {
		//Record testing: 10100, 1/1/1970, 1/1/1970, 1/1/1970, Shipped, null, 363
		int orderNumber = 10100;
		Date orderDate = this.firstDate;
		Date requiredDate = this.secondDate;
		Date shippedDate = this.thirdDate;
		String status = "Shipped";
		String comments = null;
		int customerNumber = 363;
		
		Order testOrder = new Order(orderNumber, orderDate, requiredDate, shippedDate, status, comments, customerNumber);
		
		assertEquals(orderNumber, testOrder.getOrderNumber());
		assertEquals(orderDate, testOrder.getOrderDate());
		assertEquals(requiredDate , testOrder.getRequiredDate());
		assertEquals(shippedDate, testOrder.getShippedDate());
		assertEquals(status, testOrder.getStatus());
		assertEquals(comments, testOrder.getComments());
		assertEquals(customerNumber, testOrder.getCustomerNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getOrderNumber()}.
	 */
	@Test
	public void testGetOrderNumber() {
		assertEquals(1, this.firstOrder.getOrderNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getOrderDate()}.
	 */
	@Test
	public void testGetOrderDate() {
		assertEquals(this.firstDate, this.firstOrder.getOrderDate());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getRequiredDate()}.
	 */
	@Test
	public void testGetRequiredDate() {
		assertEquals(this.secondDate, this.firstOrder.getRequiredDate());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getShippedDate()}.
	 */
	@Test
	public void testGetShippedDate() {
		assertEquals(this.thirdDate, this.firstOrder.getShippedDate());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getStatus()}.
	 */
	@Test
	public void testGetStatus() {
		assertEquals("On Hold", this.firstOrder.getStatus());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getComments()}.
	 */
	@Test
	public void testGetComments() {
		assertEquals(null, this.firstOrder.getComments());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#getCustomerNumber()}.
	 */
	@Test
	public void testGetCustomerNumber() {
		assertEquals(0, this.firstOrder.getCustomerNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#findOrdersOnStatus(java.util.List, java.lang.String)}.
	 */
	@Test
	public void testFindOrdersOnStatus() {
		//Add all orders to an array.
		List<Order> orders = new ArrayList<> ();
		orders.add(firstOrder);
		orders.add(secondOrder);
		orders.add(thirdOrder);
		
		//Find all orders with status hold.
		List<Order> ordersOnHold = Order.findOrdersOnStatus(orders, "On Hold");
		
		//Only 2 orders should be in ordersOnHold list, firstOrder and secondOrder.
		assertEquals(this.firstOrder, ordersOnHold.get(0));
		assertEquals(this.secondOrder, ordersOnHold.get(1));
		assertEquals(2, ordersOnHold.size());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Order#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("#1", this.firstOrder.toString());
	}

}
