/**
 * CustomerTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the Customer class.
 * 
 * @author felipedabrantes
 *
 */
public class CustomerTest {
	// Multiple customer objects to test for functions where a list is needed. */
	private Customer firstCustomer = null;
	private Customer secondCustomer = null;
	private Customer thirdCustomer = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.firstCustomer = new Customer(1, "Name", "Contact_Last", "Contact_First", "+111",
				"AddressLine1", "AddressLine2", "City", "State", "Postal_Code", "Country", 0, 100);
		this.secondCustomer = new Customer(2, "Name", "Contact_Last", "Contact_First", "+111",
				"AddressLine1", "AddressLine2", "City", "State", "Postal_Code", "Country", 0, 100);
		this.thirdCustomer = new Customer(3, "Name", "Contact_Last", "Contact_First", "+111",
				"AddressLine1", "AddressLine2", "City", "State", "Postal_Code", "Country", 0, 100);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.firstCustomer = null;
		this.secondCustomer = null;
		this.thirdCustomer = null;
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#Customer(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, double)}.
	 */
	@Test
	public void testCustomerConstructor() {
		/* 
		 * Record testing: 103, Atelier graphique, Schmitt, Carine, 40.32.2555, 
		 * 54\, rue Royale, null, Nantes, null, 44000, France, 1370, 21000.00
		 */
		
		int customerNumber = 103;
		String customerName = "Atelier graphique";
		String contactLastName = "Schmitt";
		String contactFirstName = "Carine ";
		String phone = "40.32.2555";
		String addressLine1 = "54, rue Royale";
		String addressLine2 = null;
		String city = "Nantes";
		String state = null;
		String postalCode = "44000";
		String country = "France";
		int salesRepEmployeeNumber = 1370;
		double creditLimit = 21000.00;
		
		Customer testCustomer = new Customer(customerNumber, customerName, contactLastName, contactFirstName, phone, 
				addressLine1, addressLine2, city, state, postalCode, country, salesRepEmployeeNumber, creditLimit);
		
		assertEquals(customerNumber, testCustomer.getCustomerNumber());
		assertEquals(customerName, testCustomer.getCustomerName());
		assertEquals(contactLastName, testCustomer.getContactLastName());
		assertEquals(contactFirstName, testCustomer.getContactFirstName());
		assertEquals(phone, testCustomer.getPhone());
		assertEquals(addressLine1, testCustomer.getAddressLine1());
		assertEquals(addressLine2, testCustomer.getAddressLine2());
		assertEquals(city, testCustomer.getCity());
		assertEquals(state, testCustomer.getState());
		assertEquals(postalCode, testCustomer.getPostalCode());
		assertEquals(country, testCustomer.getCountry());
		assertEquals(salesRepEmployeeNumber, testCustomer.getSalesRepEmployeeNumber());
		assertEquals(creditLimit, testCustomer.getCreditLimit(), 0);
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getCustomerNumber()}.
	 */
	@Test
	public void testGetCustomerNumber() {
		assertEquals(1, this.firstCustomer.getCustomerNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getCustomerName()}.
	 */
	@Test
	public void testGetCustomerName() {
		assertEquals("Name", this.firstCustomer.getCustomerName());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getContactLastName()}.
	 */
	@Test
	public void testGetContactLastName() {
		assertEquals("Contact_Last", this.firstCustomer.getContactLastName());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getContactFirstName()}.
	 */
	@Test
	public void testGetContactFirstName() {
		assertEquals("Contact_First", this.firstCustomer.getContactFirstName());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getPhone()}.
	 */
	@Test
	public void testGetPhone() {
		assertEquals("+111", this.firstCustomer.getPhone());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getAddressLine1()}.
	 */
	@Test
	public void testGetAddressLine1() {
		assertEquals("AddressLine1", this.firstCustomer.getAddressLine1());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getAddressLine2()}.
	 */
	@Test
	public void testGetAddressLine2() {
		assertEquals("AddressLine2", this.firstCustomer.getAddressLine2());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getCity()}.
	 */
	@Test
	public void testGetCity() {
		assertEquals("City", this.firstCustomer.getCity());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getState()}.
	 */
	@Test
	public void testGetState() {
		assertEquals("State", this.firstCustomer.getState());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getPostalCode()}.
	 */
	@Test
	public void testGetPostalCode() {
		assertEquals("Postal_Code", this.firstCustomer.getPostalCode());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getCountry()}.
	 */
	@Test
	public void testGetCountry() {
		assertEquals("Country", this.firstCustomer.getCountry());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getSalesRepEmployeeNumber()}.
	 */
	@Test
	public void testGetSalesRepEmployeeNumber() {
		assertEquals(0, this.firstCustomer.getSalesRepEmployeeNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getCreditLimit()}.
	 */
	@Test
	public void testGetCreditLimit() {
		assertEquals(100, this.firstCustomer.getCreditLimit(), 0);
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#findCustomer(java.util.List, int)}.
	 */
	@Test
	public void testFindCustomer() {
		List<Customer> customers = new ArrayList<Customer> ();
		customers.add(this.firstCustomer);
		customers.add(this.secondCustomer);
		customers.add(this.thirdCustomer);
		
		//Expect customer with customer 3, which is this.thirdCustomer.
		assertEquals(this.thirdCustomer, Customer.findCustomer(customers, 3));	
	}

	/**
	 * Test method for {@link com.com1028.assignment.Customer#getFullInfo()}
	 */
	@Test
	public void testGetFullInfo() {
		StringBuffer expectedFullInfo = new StringBuffer();
		
		expectedFullInfo.append("\n-> Contact:\n  - Contact_Last, Contact_First (+111)\n");
		expectedFullInfo.append("\n-> Address:\n  - AddressLine1");
		expectedFullInfo.append("\n  - AddressLine2");
		expectedFullInfo.append("\n  - City");
		expectedFullInfo.append("\n  - State");
		expectedFullInfo.append("\n  - Postal_Code");
		expectedFullInfo.append("\n  - Country");
		expectedFullInfo.append("\n\n-> Info:\n  - Sales Rep Employee Num: #0");
		expectedFullInfo.append("\n  - Credit Limit: £100.00\n");
		
		
		assertEquals(expectedFullInfo.toString(), this.firstCustomer.getFullInfo());
	}
	
	/**
	 * Test method for {@link com.com1028.assignment.Customer#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("Name (#1)", this.firstCustomer.toString());
	}

}
