/**
 * MainSystemTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the MainSystem class.
 * 
 * @author felipedabrantes
 *
 */
public class MainSystemTest {
	
	private BaseQuery baseQuery = null;
	private MainSystem mainSystem = null;

	/**
	 * Called before every test case to initialise fields and load resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.baseQuery = new BaseQuery("root", "");
		this.mainSystem = new MainSystem("root", "");
		
		this.mainSystem.addCustomersFromTable("customers");
		this.mainSystem.addPaymentsFromTable("payments");
		this.mainSystem.addOrdersFromTable("orders");
		this.mainSystem.addOrderDetailsFromTable("orderdetails");
		this.mainSystem.addProductsFromTable("products");
	}

	/**
	 * Called after the end of each test case to release resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.baseQuery = null;
		this.mainSystem = null;
	}


	/**
	 * First Requirement Test.
	 * 
	 * Test method for {@link com.com1028.assignment.MainSystem#paymentsGreaterThanTwiceTheAverageReport()}.
	 * 
	 * @throws SQLException Thrown if error with SQL statement.
	 */
	@Test
	public void testPaymentsGreaterThanTwiceTheAverageReport() throws SQLException {
		
		//---Getting payments greater than twice the average amount using only SQL.---
		ResultSet result = this.baseQuery.runQuery("" +
				"SELECT clp.customerNumber, clc.customerName, clp.checkNumber, clp.paymentDate, clp.amount " + 
				"FROM payments clp " + 
				"LEFT JOIN (SELECT customerName, customerNumber FROM classicmodels.customers) clc ON clp.customerNumber = clc.customerNumber " + 
				"HAVING clp.amount > (SELECT 2*AVG(amount) FROM payments) " + 
				"ORDER BY clp.amount;");	
		
		//---Turn SQL result into a report.---
		StringBuffer actualPaymentsGreaterReport = new StringBuffer();
		actualPaymentsGreaterReport.append("\n---Payments Greater Than Twice The Average---\n");

		while(result.next()) {
			actualPaymentsGreaterReport.append("\n");
			actualPaymentsGreaterReport.append(result.getString("checkNumber"));
			actualPaymentsGreaterReport.append(" £");
			actualPaymentsGreaterReport.append(result.getString("amount"));
			actualPaymentsGreaterReport.append(" -> ");
			actualPaymentsGreaterReport.append(result.getString("customerName"));
			actualPaymentsGreaterReport.append(" (#");
			actualPaymentsGreaterReport.append(result.getString("customerNumber"));
			actualPaymentsGreaterReport.append(")");
		}
		
		//---Fetching expected report.---
		String expectedPaymentsGreaterReport = this.mainSystem.paymentsGreaterThanTwiceTheAverageReport();
		
		//---Comparing reports.---
		assertEquals(expectedPaymentsGreaterReport, actualPaymentsGreaterReport.toString());
	}

	
	/**
	 * Second Requirement Test.
	 * 
	 * Test method for {@link com.com1028.assignment.MainSystem#ordersStatusReport(java.lang.String)}.
	 * 
	 * @throws SQLException Thrown if error with SQL statement.
	 */
	@Test
	public void testOrdersStatusReport() throws SQLException {
		String status = "On Hold";
				
		//---Getting order numbers "On Hold" and their associated customer info using only SQL.---
		ResultSet result = this.baseQuery.runQuery("" +
				"SELECT clo.orderNumber, clc.*, clo.status " + 
				"FROM classicmodels.orders clo " + 
				"LEFT JOIN (SELECT * FROM classicmodels.customers) clc ON clo.customerNumber = clc.customerNumber " + 
				"HAVING clo.status = \"" + status + "\"" +
				"ORDER BY clc.customerName;");
		
		//List of customerNumber, custmomerName, customerFullInfo and List of order numbers associated with the customer all in one hash map.
		//Hash map is a linked hash map to keep insertion order.
		Map<List<String>, List<String>> customersOnHold = new LinkedHashMap<List<String>, List<String>> ();

		//---Collecting all data into the map.---
		while(result.next()) {	
			StringBuffer customerInfo = new StringBuffer();
			customerInfo.append("\n-> Contact:\n  - ");
			customerInfo.append(result.getString("contactLastName"));
			customerInfo.append(", ");
			customerInfo.append(result.getString("contactFirstName"));
			customerInfo.append(" (");
			customerInfo.append(result.getString("phone"));
			customerInfo.append(")\n\n-> Address:");
			if(result.getString("addressLine1") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("addressLine1"));
			}
			if(result.getString("addressLine2") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("addressLine2"));
			}
			if(result.getString("city") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("city"));
			}
			if(result.getString("state") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("state"));
			}
			if(result.getString("postalCode") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("postalCode"));
			}
			if(result.getString("country") != null) {
				customerInfo.append("\n  - ");
				customerInfo.append(result.getString("country"));
			}
			customerInfo.append("\n\n-> Info:\n  - Sales Rep Employee Num: #");
			customerInfo.append(result.getString("salesRepEmployeeNumber"));
			customerInfo.append("\n  - Credit Limit: £");
			customerInfo.append(result.getString("creditLimit"));
			customerInfo.append("\n");
			
			
			List<String> ordersOnHold = new ArrayList<String> ();

			List<String> fullInfo = new ArrayList<String> ();
			fullInfo.add(result.getString("customerName"));
			fullInfo.add(result.getString("customerNumber"));
			fullInfo.add(customerInfo.toString());
			
			//Checks if customer already has orders associated to it.
			if(customersOnHold.get(fullInfo) == null) {
				//If it doesn't, then add a new list of just the associated order.
				ordersOnHold.add(result.getString("orderNumber"));
				customersOnHold.put(fullInfo, ordersOnHold);
			}
			else {
				//If it does, then collect orders already associated and add the new one.
				ordersOnHold = customersOnHold.get(fullInfo);
				ordersOnHold.add(result.getString("orderNumber"));
				customersOnHold.put(fullInfo, ordersOnHold);
				
			}
		}
	
		//Writing report with SQL data.
		StringBuffer actualCustomersOrdersOnHoldReport = new StringBuffer();
		actualCustomersOrdersOnHoldReport.append("\n-*-*-*- Orders " + status + " Report -*-*-*-");
		actualCustomersOrdersOnHoldReport.append("\n\n-> Total number of customers with Orders " + status + ": ");
		actualCustomersOrdersOnHoldReport.append(customersOnHold.size());
		actualCustomersOrdersOnHoldReport.append("\n");
		
		for(Entry<List<String>, List<String>> entry: customersOnHold.entrySet()) {
			actualCustomersOrdersOnHoldReport.append("\n\n--* ");
			actualCustomersOrdersOnHoldReport.append(entry.getKey().get(0));
			actualCustomersOrdersOnHoldReport.append(" (#");
			actualCustomersOrdersOnHoldReport.append(entry.getKey().get(1));
			actualCustomersOrdersOnHoldReport.append(") *--\n-> ");
			actualCustomersOrdersOnHoldReport.append(entry.getValue().size());
			actualCustomersOrdersOnHoldReport.append(" Orders " + status + ":");
			
			for(String order: entry.getValue()) {
				actualCustomersOrdersOnHoldReport.append("\n  - #");
				actualCustomersOrdersOnHoldReport.append(order);
			}
			actualCustomersOrdersOnHoldReport.append("\n");
			actualCustomersOrdersOnHoldReport.append(entry.getKey().get(2));
		}
		
		//---Fetching expected report.---
		String expectedCustomersOrdersOnHoldReport = this.mainSystem.ordersStatusReport(status);
		
		//---Comparing reports.---
		assertEquals(expectedCustomersOrdersOnHoldReport.toString(), actualCustomersOrdersOnHoldReport.toString());
	}

	
	/**
	 * Third Requirement Test.
	 * 
	 * Test method for {@link com.com1028.assignment.MainSystem#findRevenueOfProducts()}.
	 * 
	 * @throws SQLException Thrown if error with SQL statement.
	 */
	@Test
	public void testFindRevenueOfProducts() throws SQLException {
		//---Getting total revenue made from each product using SQL.---		
		Map<String, Double> actualProductRevenue = new LinkedHashMap<String, Double> ();
		
		ResultSet result = this.baseQuery.runQuery("SELECT clod.productCode, clp.productName, SUM(clod.quantityOrdered * clod.priceEach) AS productRevenue "
				+ "FROM classicmodels.orderdetails clod "
				+ "LEFT JOIN (SELECT productCode, productName FROM classicmodels.products) clp ON clod.productCode = clp.productCode "
				+ "GROUP BY clod.productCode "
				+ "ORDER BY clp.productName;");
		
		while(result.next()) {
			String productDescription = result.getString("productName") + " (#" + result.getString("productCode") + ")";
			actualProductRevenue.put(productDescription, result.getDouble("productRevenue"));
		}
		
		//---Writing report from actual SQL data.---
		StringBuffer report = new StringBuffer();
		report.append("\n---Product Revenue---\n");
		for(Entry<String, Double> entry: actualProductRevenue.entrySet()) {
			report.append("\n");
			report.append(entry.getKey());
			report.append(" -> £");
			report.append(Payment.PRICE_FORMAT.format(entry.getValue()));
		}
		
		//Comparing actual report to expected report.
		assertEquals(this.mainSystem.revenueOfProductsReport(), report.toString());
	}

}
