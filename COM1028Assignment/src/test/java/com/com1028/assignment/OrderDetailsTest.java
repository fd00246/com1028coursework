/**
 * OrderDetailsTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the OrderDetails class.
 * 
 * @author felipedabrantes
 *
 */
public class OrderDetailsTest {
	
	private OrderDetails firstOrderDetails = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.firstOrderDetails = new OrderDetails(0, "A", 50, 10.00, 1);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.firstOrderDetails = null;
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#OrderDetails(int, java.lang.String, int, double, int)}.
	 */
	@Test
	public void testOrderDetailsConstructor() {
		//Record tested: 10100, S18_1749, 30, 136.00, 3
		int orderNumber = 10100;
		String productCode = "S18_1749";
		int quantityOrdered = 30;
		double priceEach = 136.00;
		int orderLineNumber = 3;
		
		OrderDetails testOrderDetails = new OrderDetails(orderNumber, productCode, quantityOrdered, 
				priceEach, orderLineNumber);
		
		assertEquals(orderNumber, testOrderDetails.getOrderNumber());
		assertEquals(productCode, testOrderDetails.getProductCode());
		assertEquals(quantityOrdered, testOrderDetails.getQuantityOrdered());
		assertEquals(priceEach, testOrderDetails.getPriceEach(), 0);
		assertEquals(orderLineNumber, testOrderDetails.getOrderLineNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#getOrderNumber()}.
	 */
	@Test
	public void testGetOrderNumber() {
		assertEquals(0, this.firstOrderDetails.getOrderNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#getProductCode()}.
	 */
	@Test
	public void testGetProductCode() {
		assertEquals("A", this.firstOrderDetails.getProductCode());
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#getQuantityOrdered()}.
	 */
	@Test
	public void testGetQuantityOrdered() {
		assertEquals(50, this.firstOrderDetails.getQuantityOrdered());
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#getPriceEach()}.
	 */
	@Test
	public void testGetPriceEach() {
		assertEquals(10.00, this.firstOrderDetails.getPriceEach(), 0);
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#getOrderLineNumber()}.
	 */
	@Test
	public void testGetOrderLineNumber() {
		assertEquals(1, this.firstOrderDetails.getOrderLineNumber());
	}

	/**
	 * Test method for {@link com.com1028.assignment.OrderDetails#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("#0 -> Product: #A", this.firstOrderDetails.toString());
	}

}
