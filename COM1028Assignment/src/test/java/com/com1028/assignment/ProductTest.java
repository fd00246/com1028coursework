/**
 * ProductTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the Product class.
 * 
 * @author felipedabrantes
 *
 */
public class ProductTest {
	
	private Product firstProduct = null;
	private Product secondProduct = null;
	private Product thirdProduct = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.firstProduct = new Product("A", "Name1", "Line", "Scale", "Vendor", "Desc", 10, 10, 100);
		this.secondProduct = new Product("B", "Name2", "Line", "Scale", "Vendor", "Desc", 10, 20, 200);
		this.thirdProduct = new Product("C", "Name3", "Line", "Scale", "Vendor", "Desc", 10, 30, 300);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.firstProduct = null;
		this.secondProduct = null;
		this.thirdProduct = null;
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#Product(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, double, double)}.
	 */
	@Test
	public void testProductConstructor() {		
		/* Record testing: S10_1678, 1969 Harley Davidson Ultimate Chopper, Motorcycles, 1:10, Min Lin Diecast, 
		 * longDescription, 7933, 48.81, 95.70
		 */
		
		String productCode = "S10_1678";
		String productName = "1969 Harley Davidson Ultimate Chopper";
		String productLine = "Motorcycles";
		String productScale = "1:10";
		String productVendor = "Min Lin Diecast";
		String productDescription = "longDescription";
		int quantityInStock = 7933;
		double buyPrice = 48.81;
		double MSRP = 95.70;
		
		Product testProduct = new Product(productCode, productName, productLine, productScale, 
				productVendor, productDescription, quantityInStock, buyPrice, MSRP);
		
		assertEquals(productCode, testProduct.getProductCode());
		assertEquals(productName, testProduct.getProductName());
		assertEquals(productLine, testProduct.getProductLine());
		assertEquals(productScale, testProduct.getProductScale());
		assertEquals(productVendor, testProduct.getProductVendor());
		assertEquals(productDescription, testProduct.getProductDescription());
		assertEquals(quantityInStock, testProduct.getQuantityInStock());
		assertEquals(buyPrice, testProduct.getBuyPrice(), 0);
		assertEquals(MSRP, testProduct.getMSRP(), 0);
		
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductCode()}.
	 */
	@Test
	public void testGetProductCode() {
		assertEquals("A", this.firstProduct.getProductCode());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductName()}.
	 */
	@Test
	public void testGetProductName() {
		assertEquals("Name1", this.firstProduct.getProductName());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductLine()}.
	 */
	@Test
	public void testGetProductLine() {
		assertEquals("Line", this.firstProduct.getProductLine());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductScale()}.
	 */
	@Test
	public void testGetProductScale() {
		assertEquals("Scale", this.firstProduct.getProductScale());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductVendor()}.
	 */
	@Test
	public void testGetProductVendor() {
		assertEquals("Vendor", this.firstProduct.getProductVendor());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getProductDescription()}.
	 */
	@Test
	public void testGetProductDescription() {
		assertEquals("Desc", this.firstProduct.getProductDescription());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getQuantityInStock()}.
	 */
	@Test
	public void testGetQuantityInStock() {
		assertEquals(10, this.firstProduct.getQuantityInStock());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getBuyPrice()}.
	 */
	@Test
	public void testGetBuyPrice() {
		assertEquals(10, this.firstProduct.getBuyPrice(), 0);
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#getMSRP()}.
	 */
	@Test
	public void testGetMSRP() {
		assertEquals(100, this.firstProduct.getMSRP(), 0);
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#findProduct(java.util.List, java.lang.String)}.
	 */
	@Test
	public void testFindProduct() {
		List<Product> products = new ArrayList<Product> ();
		products.add(this.firstProduct);
		products.add(this.secondProduct);
		products.add(this.thirdProduct);
		
		//Expect product with customer 3, which is this.thirdCustomer.
		assertEquals(this.thirdProduct, Product.findProduct(products, "C"));
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("Name1 (#A)", this.firstProduct.toString());
	}

	/**
	 * Test method for {@link com.com1028.assignment.Product#compareTo(com.com1028.assignment.Product)}.
	 */
	@Test
	public void testCompareTo() {
		//The name of firstProduct is (alphabetically) less than name of secondProduct.
		//Name1 < Name2.
		//If less than, then compare function returns -1.
		assertEquals(-1, this.firstProduct.compareTo(secondProduct));
	}

}
