/**
 * PaymentTest.java
 */
package com.com1028.assignment;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the Payment class.
 * 
 * @author felipedabrantes
 *
 */
public class PaymentTest {
	//Different payments needed to test average functions.
	private Payment firstPayment = null;
	private Payment secondPayment = null;
	private Payment thirdPayment = null;
	
	/** Date to test payment methods. 1 second after 1/1/1970. */
	private Date date = new Date(1000);
	
	
	/**
	 * Called before every test case to initialise fields and load resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.firstPayment = new Payment(0, "AAA", this.date, 100.0);
		this.secondPayment = new Payment(0, "BBB", this.date, 150.0);
		this.thirdPayment = new Payment(0, "CCC", this.date, 650.0);
	}

	
	/**
	 * Called after the end of each test case to release resources.
	 * 
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.firstPayment = null;
		this.secondPayment = null;
		this.thirdPayment = null;
	}
	
	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#Payment(int customerNumber, 
	 * String checkNumber, Date paymentDate, double amount)}.
	 */
	@Test
	public void testPaymentConstructor() {
		//Record testing: 103, HQ336336, 1/1/1970, 6066.78
		int customerNumber = 103;
		String checkNumber = "HQ336336";
		Date paymentDate = this.date;
		Double amount = 6066.78;
		
		Payment testPayment = new Payment(customerNumber, checkNumber, paymentDate, amount);
		
		assertEquals(customerNumber, testPayment.getCustomerNumber());
		assertEquals(checkNumber, testPayment.getCheckNumber());
		assertEquals(paymentDate , testPayment.getPaymentDate());
		assertEquals(amount, testPayment.getAmount(), 0);
	}
	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#getCustomerNumber()}.
	 */
	@Test
	public void testGetCustomerNumber() {
		assertEquals(0, this.firstPayment.getCustomerNumber());
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#getCheckNumber()}.
	 */
	@Test
	public void testGetCheckNumber() {
		assertEquals("AAA", this.firstPayment.getCheckNumber());
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#getPaymentDate()}.
	 */
	@Test
	public void testGetPaymentDate() {
		assertEquals(new Date(1000), this.firstPayment.getPaymentDate());
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#getAmount()}.
	 */
	@Test
	public void testGetAmount() {
		assertEquals(100.0, this.firstPayment.getAmount(), 0);
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#findAverageAmount(java.util.List)}.
	 */
	@Test
	public void testFindAverageAmount() {
		List<Payment> paymentsList = new ArrayList<> ();
		
		//Has 100 Amount.
		paymentsList.add(firstPayment);
		//Has 150 Amount.
		paymentsList.add(secondPayment);
		//Has 650 Amount.
		paymentsList.add(thirdPayment);
		
		double expectedAverage = (100 + 150 + 650)/3.0;
		
		assertEquals(expectedAverage, Payment.findAverageAmount(paymentsList), 0);
	}

	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#findGreaterThanAverage(java.util.List, double)}.
	 */
	@Test
	public void testFindGreaterThanAverage() {
		List<Payment> paymentsList = new ArrayList<> ();
		
		//Has 100 Amount.
		paymentsList.add(firstPayment);
		//Has 150 Amount.
		paymentsList.add(secondPayment);
		//Has 650 Amount.
		paymentsList.add(thirdPayment);
		
		//Third payment is at least two times bigger than average.
		List<Payment> expectedPayments = new ArrayList<> ();
		expectedPayments.add(thirdPayment);
		
		assertEquals(expectedPayments, Payment.findGreaterThanAverage(paymentsList, 2));	
	}
	
	
	/**
	 * Test method for {@link com.com1028.assignment.Payment#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("AAA £100.00", this.firstPayment.toString());
	}
	
	@Test
	public void testCompareTo() {
		//Compares amount attribute of payments.
		//If payment amount in parameter is bigger, return -1. If it is smaller, return 1, if it is the same, return 0.
		//firstPayment = 100, secondPayment = 150. Therefore expect to return -1 as 100 < 150.
		assertEquals(-1, this.firstPayment.compareTo(secondPayment));
	}

}
